<?php

/**
 * @file
 * Integrates JQuery menu functionality to create a click and expand book block.
 */

/**
 * Implements hook_menu().
 */
function advancedbookblocks_menu() {
  $items['admin/config/user-interface/advanced-book-blocks'] = array(
    'title'            => 'Advanced Book Blocks',
    'description'      => 'Add and configure Advanced Book Blocks.',
    'page callback'    => 'book_blocks_list',
    'access arguments' => array('administer menu'),
    'type'             => MENU_NORMAL_ITEM,
    'file' => 'advancedbookblocks.admin.inc',
  );
  $items['admin/config/user-interface/advanced-book-blocks/list'] = array(
    'title'  => 'List',
    'type'   => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/config/user-interface/advanced-book-blocks/add'] = array(
    'title' => 'Add New Book Block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('book_blocks_add'),
    'access arguments' => array('administer menu'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'advancedbookblocks.admin.inc',
  );
  $items['admin/config/user-interface/advanced-book-blocks/configure'] = array(
    'title' => 'Configure',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('book_blocks_config', 1),
    'access arguments' => array('administer taxonomy'),
    'type' => MENU_CALLBACK,
    'file' => 'advancedbookblocks.admin.inc',
  );
  $items['admin/config/user-interface/advanced-book-blocks/delete'] = array(
    'title' => 'Configure',
    'page callback' => 'book_blocks_delete',
    'access arguments' => array('administer taxonomy'),
    'type' => MENU_CALLBACK,
    'file' => 'advancedbookblocks.admin.inc',
  );
  $items['admin/config/user-interface/advanced-book-blocks/reset'] = array(
    'title' => 'Configure',
    'page callback' => 'book_blocks_reset',
    'access arguments' => array('administer taxonomy'),
    'type' => MENU_CALLBACK,
    'file' => 'advancedbookblocks.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function advancedbookblocks_block_info() {
  $result = db_query("SELECT delta FROM {adv_book_block}");
  foreach ($result as $book) {
    $delta_array[] = $book->delta;
  }
  if (!empty($delta_array)) {
    foreach ($delta_array as $d) {
      $block_title = '';
      $block_title = db_query("SELECT title FROM {adv_book_block} WHERE delta = :delta", array(':delta' => $d))->fetchField();
      $blocks[$d]['info'] = $block_title . '- Advanced book block';
      $blocks[$d]['cache'] = DRUPAL_CACHE_PER_ROLE | DRUPAL_CACHE_PER_PAGE;
    }
  }
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function advancedbookblocks_block_view($delta = '') {
  $block = array();
  $result = db_query("SELECT delta FROM {adv_book_block}");
  foreach ($result as $book) {
    $delta_array[] = $book->delta;
  }
  foreach ($delta_array as $d) {
    if ($delta == $d) {
      $toplevel = books_get_top_level($d, 'display');
      if (!empty($toplevel)) {
        $tree = recursive_book_array_builder($toplevel);
        $trail = book_trail_builder();

        if (empty($trail)) {
          $trail = array();
        }
        $block_title = '';
        $block_title = db_query("SELECT title FROM {adv_book_block} WHERE delta = :delta", array(':delta' => $d))->fetchField();
        $block['subject'] = $block_title;
        $block['content'] = theme('jquerymenu_menu', array('tree' => $tree, 'trail' => $trail));
      }
    }
  }
  return $block;
}

function book_trail_builder() {
  $trail = array();
  if (arg(0) == 'node') {
    $nid = arg(1);

    if (is_numeric($nid)) {
        $node = node_load($nid);
    }
    //check to see if node is a book
    if (!empty($node->book['bid'])) {
      // if node is top level book go no further.
      if ($node->nid != $node->book['bid']) {

        $parents = book_get_all_parents($node->book['mlid']);
        if (!empty($parents)) {
          foreach ($parents as $parent) {
            $query = db_select('node', 'n');
            $query->join('book', 'b', 'b.nid = n.nid');
            $parent_nid = $query
              ->fields('b', array('nid'))
              ->condition('b.mlid', $parent)
              ->condition('n.status', 1)
              ->addTag('node_access')
              ->execute()
              ->fetchField();
            if (!empty($parent_nid)) {
              $trail[] = 'node/' . $parent_nid;
            }
          }
        }
      }
    }
  }
  return $trail;
}

function book_get_all_parents($mlid) {
  $parent = '';
  $parents = array();
  $parent = db_query("SELECT plid FROM {menu_links} WHERE mlid = :mlid", array(':mlid' => $mlid))->fetchField();
  if (!empty($parent)) {
    $parents[] = $parent;
    $next = book_get_all_parents($parent);
    if (!empty($next)) {
      $parents = array_merge($parents, $next);
    }
  }
  return $parents;
}

function books_get_top_level($d = NULL, $context = NULL) {
  $books = array();
  $result = '';
  if ($d == NULL && $context == NULL) {
    $query = db_select('book', 'b');
    $query->join('menu_links', 'm',  'm.mlid = b.mlid');
    $result = $query
      ->fields('b', array('bid'))
      ->fields('m', array('weight'))
      ->orderBy('m.weight', 'ASC')
      ->addTag('node_access')
      ->execute();
  }
  if ($d != NULL && $context == NULL) {
    $result = db_query("SELECT bid, weight FROM {adv_book_custom} WHERE delta = :delta ORDER BY weight ASC", array(':delta' => $d));
  }
  if ($d != NULL && $context == 'display') {
    $result = db_query("SELECT bid, weight FROM {adv_book_custom} WHERE delta = :delta AND enabled = 1 ORDER BY weight ASC", array(':delta' => $d));
  }
  foreach ($result as $book) {
    if (!in_array($book->bid, $books)) {
      $books[] = $book->bid;
    }
  }
  return $books;
}

function get_book_children($mlid) {
  $nid_array = array();
  $result = db_query("SELECT mlid, weight, link_title FROM {menu_links} WHERE plid = :plid ORDER by weight, link_title", array(':plid' => $mlid));
  foreach ($result as $child) {
    if (!empty($child->mlid)) {
      $mlid_array[] = $child->mlid;
    }
  }
  if (!empty($mlid_array )) {
    foreach ($mlid_array as $child_mlid) {
      # We need the join with the node table here in order to let the i18n
      # module's rewriting kick in.
      $query = db_select('node', 'n');
      $query->join('book', 'b', 'b.nid = n.nid');
      $child_nid = $query
        ->fields('n', array('nid'))
        ->condition('b.mlid', $child_mlid)
        ->addTag('node_access')
        ->execute()
	    ->fetchField();
      if (!empty($child_nid)) {
        $nid_array[] = $child_nid;
      }
    }
  }
  return $nid_array;
}

function recursive_book_array_builder($nid_array) {
  foreach ($nid_array as $nid) {
    $query = db_select('node', 'n');
    $query->join('book', 'b', 'b.nid = n.nid');
    $result = $query
	  ->fields('n', array('title', 'status'))
      ->fields('b', array('mlid'))
	  ->condition('n.nid', $nid)
      ->addTag('node_access')
      ->execute();
    foreach ($result as $book) {
      $book_title  = $book->title;
      $status      = $book->status;
      $mlid        = $book->mlid;
    }
    if ($status == 1) {
      $tree[$mlid]['link']['title'] = $book_title ;
      $tree[$mlid]['link']['hidden'] = '0';
      $tree[$mlid]['link']['href'] = 'node/'. $nid;
      $tree[$mlid]['link']['options']['attributes']['title'] = $book_title;
    }
    else {
      $tree[$mlid]['link']['hidden'] = '1';
    }
    $children = array();
    $tree[$mlid]['below'] = array();
    $children = get_book_children($mlid);
    if (!empty($children)) {
      $tree[$mlid]['link']['has_children'] = '1';

      $subarray = recursive_book_array_builder($children);
      if (!empty($subarray)) {
        $tree[$mlid]['below'] = array_merge($tree[$mlid]['below'], $subarray);
      }
      else {
        $tree[$mlid]['below'] = FALSE;
      }
    }
    else {
      $tree[$mlid]['link']['has_children'] = '0';
      $tree[$mlid]['below'] = FALSE;
    }
  }
  return $tree;
}

/**
 * Implements hook_theme().
 */
function advancedbookblocks_theme() {
  return array(
    'book_blocks_config_list' => array(
      'render element' => 'form'
    ),
  );
}

/**
 * Implements hook_module_implements_alter().
 */
function advancedbookblocks_module_implements_alter(&$implementations, $hook) {
  switch ($hook) {
    case 'node_insert':
      $b = $implementations['book'];
      unset($implementations['book']);
      $a = $implementations['advancedbookblocks'];
      unset($implementations['advancedbookblocks']);
      $implementations['book'] = $b;
      $implementations['advancedbookblocks'] = $a;
      break;
    case 'node_update':
      $b = $implementations['book'];
      unset($implementations['book']);
      $a = $implementations['advancedbookblocks'];
      unset($implementations['advancedbookblocks']);
      $implementations['book'] = $b;
      $implementations['advancedbookblocks'] = $a;
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function advancedbookblocks_form_book_outline_form_alter(&$form, &$form_state, $form_id) {
  $form['#submit'][] = 'advancedbookblocks_outline_form_submit';
}

function advancedbookblocks_outline_form_submit($form, &$form_state) {
  $node = $form['#node'];
  if($node->book['plid'] == 0) {
    $result = db_query("SELECT enabled FROM {adv_book_custom} WHERE bid = :bid", array(':bid' => $node->book['bid']))->fetchField();
      if ($result !== FALSE) {
        _advancedbookblocks_node_op($node, 'update');
      }
      else {
        _advancedbookblocks_node_op($node, 'insert');
      }
  }
}

/**
 * Implements hook_node_update().
 */
function advancedbookblocks_node_update($node) {
  _advancedbookblocks_node_op($node, 'update');
}

/**
 * Implements hook_node_insert().
 */
function advancedbookblocks_node_insert($node) {
  _advancedbookblocks_node_op($node, 'insert');
}

/**
 * Implements hook_node_delete().
 */
function advancedbookblocks_node_delete($node) {
  _advancedbookblocks_node_op($node, 'delete');
}

function _advancedbookblocks_node_op($node, $op) {
  $node_terms = array();
  $bid = isset($node->book['bid']) ? $node->book['bid'] : '';
  if ($op == 'update' || $op == 'insert' ) {

    if (!empty($bid) && ($bid == $node->nid)) {
      $result = db_query("SELECT delta FROM {adv_book_block} WHERE custom = 1");
      foreach ($result as $block) {
        $customized_blocks[] = $block->delta;
      }
      if (!empty($customized_blocks)) {
        $result = db_select('taxonomy_index', 'ti')
          ->fields('ti', array('tid'))
          ->condition('nid', $bid)
          ->addTag('node_access')
          ->execute();
        foreach ($result as $term) {
          $node_terms[] = $term->tid;
        }
        foreach ($customized_blocks as $delta) {
          $tid_string = db_query("SELECT tids FROM {adv_book_block} WHERE delta = :delta", array(':delta' => $delta))->fetchField();
          $tid_trig_array = explode(',', $tid_string);
          $enable_setting = '';
          $enable_result = '';
          $enable_result = db_query("SELECT add_mode FROM {adv_book_block} WHERE delta = :delta", array(':delta' => $delta))->fetchField();
          $has_bid = '';
          $has_bid = db_query("SELECT enabled FROM {adv_book_custom} WHERE bid = :bid AND delta = :delta", array(':bid' => $bid, ':delta' => $delta))->fetchField();
          $check = array();
          if (is_array($tid_trig_array) && is_array($node_terms)) {
            if (array_intersect($node_terms,$tid_trig_array) != NULL) {
              $check[] = array_intersect($node_terms, $tid_trig_array);
            }
          }
          switch ($enable_result) {
            case '0':
              if ($op == 'update') {
                $enable_setting = 1;
              }
              if ($op == 'insert') {
                $enable_setting = 0;
              }
            break;

            case '1':
              $enable_setting = 1;
            break;

            case '2':
              if (!empty($check)) {
                $enable_setting = 1;
              }
              if (empty($check)) {
                $enable_setting = 0;
              }
            break;
            case '3':
              if (!empty($check)) {
                $enable_setting = 0;
              }
              if (empty($check)) {
                $enable_setting = 1;
              }
            break;
            case '4':
              $enable_setting = 0;
              if ($has_bid) $enable_setting = $has_bid;
          }
          if ($has_bid == NULL) {
            db_insert('adv_book_custom')
              ->fields(array(
                'delta' => $delta,
                'bid' => $bid,
                'enabled' => $enable_setting,
                'weight' => $node->book['weight'],
              ))
              ->execute();
          }
          if ($has_bid != NULL) {
            db_update('adv_book_custom')
              ->fields(array('enabled' => $enable_setting))
              ->condition('delta', $delta)
              ->condition('bid', $bid)
              ->execute();
          }
        }
      }
    }
  }
  if ($op == 'delete') {
    if (!empty($bid) && ($bid == $node->nid)) {
      $result = db_query("SELECT delta FROM {adv_book_block} WHERE custom = 1");
      foreach ($result as $block) {
        $customized_blocks[] = $block->delta;
      }
      foreach ($customized_blocks as $delta) {
        db_delete('adv_book_custom')
          ->condition('delta', $delta)
          ->condition('bid', $bid)
          ->execute();
      }
    }
  }
}
