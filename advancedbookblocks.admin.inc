<?php

/**
 * @file
 * Admin page callbacks for the advancedbookblocks module.
 */

function book_blocks_delete($id_token) {
  $id_token = explode('-', $id_token);
  $delta = $id_token[0];
  $token = $id_token[1];
  $valid = drupal_valid_token($token, 'abb_del');
  if ($valid) {
    if ($delta != 0) {
      db_delete('adv_book_block')
        ->condition('delta', $delta)
        ->execute();
      db_delete('adv_book_custom')
        ->condition('delta', $delta)
        ->execute();
      drupal_set_message(t('Your block has been deleted.'), $type = 'status', $repeat = FALSE);
      drupal_goto('admin/config/user-interface/advanced-book-blocks');
    }
    else{
      drupal_set_message(t('You cannot delete the default block'), $type = 'error', $repeat = FALSE);
      drupal_goto('admin/config/user-interface/advanced-book-blocks');
    }
  }
  else {
    drupal_access_denied();
  }
}

function book_blocks_reset($id_token) {
  $id_token = explode('-', $id_token);
  $id = $id_token[0];
  $token = $id_token[1];
  $valid = drupal_valid_token($token, 'abb_del');
  if ($valid) {
    db_update('adv_book_block')
      ->fields(array('tids' => NULL, 'custom' => 0))
      ->condition('delta', $id)
      ->execute();
    db_delete('adv_book_custom')
      ->condition('delta', $id)
      ->execute();
    drupal_set_message(t('Your block has been reset.'), $type = 'status', $repeat = FALSE);
    drupal_goto('admin/config/user-interface/advanced-book-blocks');
  }
  else {
    drupal_access_denied();
  }
}

function book_blocks_list() {
  $header = array(t('Block title'), t('Actions'));
  $rows    = array();
  $all_books = book_get_books();
  if ($all_books) {
    $token = drupal_get_token('abb_del');
    $result = db_query("SELECT delta, title FROM {adv_book_block}");
    foreach ($result as $book) {
      $row = array();
      $links = array();
      $row[]   = $book->title;
      $links[] = l(t('Configure'), 'admin/config/user-interface/advanced-book-blocks/configure/' . $book->delta);
      $links[] = l(t('Reset'), 'admin/config/user-interface/advanced-book-blocks/reset/' . $book->delta . '-' . $token);
      if ($book->delta != 0) {
        $links[] = l(t('Delete'), 'admin/config/user-interface/advanced-book-blocks/delete/' . $book->delta . '-' . $token);
      }
      $row[]   = implode('&nbsp;&nbsp;&nbsp;&nbsp;', $links);
      $rows[]  = $row;
    }
  }
  $output  = theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No books available.')));

  return $output;
}

function book_blocks_add($form, &$form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#size' => '64',
    '#title' => t('New Book Block Title'),
    '#default_value' => '',
    '#description' => t('This will create a new book block you can then customize.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create New Block'),
  );
  return $form;
}

function book_blocks_add_submit($form, &$form_state) {
  $title = check_plain($form_state['values']['title']);
  $result = db_query("SELECT delta FROM {adv_book_block}");
  $x = 0;
foreach ($result as $book) {
    if ($x <= $book->delta) {
      $x = $book->delta;
    }
  }
  $delta = $x + 1;
  db_insert('adv_book_block')
    ->fields(array(
      'delta' => $delta,
      'title' => $title,
      'custom' => 0,
    ))
    ->execute();
  drupal_set_message(t('Your block has been added'), $type = 'status', $repeat = FALSE);
  drupal_goto('admin/config/user-interface/advanced-book-blocks/configure/' . $delta);
}

function book_blocks_config($form, &$form_state) {
  $args = func_get_args();
  $delta = (int)$args[3];
  $term_array = array();
  $tid_checked = array();
  $term_def = NULL;
  $form['ab_config_form'] = array('#tree' => TRUE);
  $book_array = books_get_top_level(NULL, NULL);
  $result = db_query("SELECT * FROM {adv_book_block} WHERE delta = :delta", array(':delta' => $delta));
  foreach ($result as $block) {
    $custom    = $block->custom;
    $omit_nids = $block->omit_nids;
    $tids      = $block->tids;
    $main_title_def = $block->title;
  }
  $bid_array = array();
  if ($custom == 1) {
    $result = db_query("SELECT bid FROM {adv_book_custom} WHERE delta = :delta", array(':delta' => $delta));
    foreach ($result as $enabled) {
      $enabled_array[] = $enabled->bid;
    }
  }

  if ($custom == 0) {
    $book_array = books_get_top_level(NULL, NULL);
    $add_mode_def = 1;
  }
  else {
    $book_array = books_get_top_level($delta);
    $add_mode_def = db_query("SELECT add_mode FROM {adv_book_block} WHERE delta = :delta", array(':delta' => $delta))->fetchField();
    $enabled_array = array();
    $result = db_query("SELECT bid FROM {adv_book_custom} WHERE delta = :delta AND enabled = 1", array(':delta' => $delta));
    foreach ($result as $enabled) {
      $enabled_array[] = $enabled->bid;
    }
    $tid_checked = explode(',', $tids);
  }
  foreach ($book_array as $bid) {

    if (!empty($enabled_array)) {
      if (in_array($bid, $enabled_array)) {
        $enabled_def = TRUE;
      }
      else {
        $enabled_def = FALSE;
      }
    }
    else {
      $enabled_def = FALSE;
    }
    $weight_def = db_query("SELECT weight FROM {adv_book_custom} WHERE delta = :delta AND bid = :bid", array(':delta' => $delta, ':bid' => $bid))->fetchField();
    if (empty($weight_def)) {
      $mlid = db_select('book', 'b')
        ->fields('b', array('mlid'))
        ->condition('bid', $bid)
        ->condition('nid', 'bid')
        ->addTag('node_access')
        ->execute()
        ->fetchField();
      $weight_def = db_query("SELECT weight FROM {menu_links} WHERE mlid = :mlid", array(':mlid' => $mlid))->fetchField();
    }
    $title = db_select('node', 'n')
      ->fields('n', array('title'))
      ->condition('nid', $bid)
      ->addTag('node_access')
      ->execute()
      ->fetchField();
    $form['ab_config_form']['book'][$bid]['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => l($title, 'node/' . $bid),
      '#default_value' => $enabled_def,
    );
    $form['ab_config_form']['book'][$bid]['weight'] = array(
      '#type' => 'weight',
      '#delta' => 15,
      '#default_value' => $weight_def,
    );
    $form['ab_config_form']['book'][$bid]['weight_reset'] = array(
      '#type' => 'checkbox',
      '#default_value' => FALSE,
    );
  }
  $form['ab_config_form']['#theme'] = 'book_blocks_config_list';
  $form['ab_config_form']['delta']  = array(
    '#type' => 'value',
    '#value' => $delta,
  );
  $form['ab_config_form']['book_array']  = array(
    '#type' => 'value',
    '#value' => $book_array,
  );
  $form['ab_config_form']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Main Block title'),
    '#required' => TRUE,
    '#default_value' => $main_title_def,
  );
  $form['ab_config_form']['add_mode'] = array(
    '#type' => 'select',
    '#title' => t('Automation Settings'),
    '#options' => array(t('New books are disabled by default (editing an existing book will enable it or leave it enabled)'), t('Automatically enable new books'), t('Automatically enable books with selected terms'), t('Automatically exclude books with selected terms'), t('Lock this block to any new books')),
    '#required' => TRUE,
    '#description' => t('This setting only applies to top level books and only in this particular block. Child books will be automatically added and enabled if their parent is enabled. Automatic enable or exclude by taxonomy term can be used to determine which books will be automatically enabled in which blocks. Note that this setting only works if you check at least one term in the form below. By default this will only alter the enable settings when books are added or edited, however if you want to apply this setting in mass you can select the "Mass Enable/Disable" checkbox below.'),
    '#default_value' => $add_mode_def,
  );
  $form['ab_config_form']['vocab_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Taxonomy Automation Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $form['ab_config_form']['vocab_options']['vocab'] = array('#tree' => TRUE);
  $result = db_query("SELECT vid, name FROM {taxonomy_vocabulary} WHERE name <> 'Forums'");
  foreach ($result as $vocab) {
    $vocab_array[] = $vocab->vid;
    $form['ab_config_form']['vocab_options']['vocab'][$vocab->vid]= array(
      '#type' => 'fieldset',
      '#title' => check_plain($vocab->name),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );

    $sub_result = db_select('taxonomy_term_data', 'ttd')
      ->fields('ttd', array('tid', 'name'))
      ->condition('vid', $vocab->vid)
      ->addTag('term_access')
      ->execute();
    foreach ($sub_result as $term) {
      $term_array[] = $term->tid;
      if (is_array($tid_checked)) {
        if (in_array($term->tid, $tid_checked) == TRUE) {
          $term_def = TRUE;
        }
        else {
          $term_def = FALSE;
        }
      }
      $form['ab_config_form']['vocab_options']['vocab'][$vocab->vid]['terms'][$term->tid] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($term->name),
        '#default_value' => $term_def,
      );

    }
    $form['ab_config_form']['vocab_options']['vocab'][$vocab->vid]['term_array']= array(
      '#type' => 'value',
      '#value' => $term_array,
    );
  }
  $form['ab_config_form']['vocab_array'] = array(
    '#type' => 'value',
    '#value' => $vocab_array,
  );
  $form['ab_config_form']['set_now'] = array(
    '#title' => t('Mass Enable/Disable'),
    '#type' => 'checkbox',
    '#default_value' => FALSE,
    '#description' => t('Use taxonomy automation options to enable and disable books from this block right now. If selected all books will be queried by taxonomy term and by your chosen automation settings and disabled or enabled automatically. This will override manually enabled or disabled books. This setting only affects this block.'),
  );

  $form['ab_config_form']['submit']  = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function theme_book_blocks_config_list($variables) {
  $form = $variables['form'];
  $delta = $form['delta']['#value'];
  $header = array(t('Enabled Books'), t('Weight'), t('Reset Weight'));
  $rows = array();
  $book_array = $form['book_array']['#value'];
  $output = drupal_render($form['title']);

  foreach ($book_array as $bid) {
    $row  = array();
    $row[]  = drupal_render($form['book'][$bid]['enabled']);
    $row[]  = drupal_render($form['book'][$bid]['weight']);
    $row[]  = drupal_render($form['book'][$bid]['weight_reset']);
    $rows[] = $row;
  }
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}

function book_blocks_config_submit($form, &$form_state) {
  $tid_string = NULL;
  $set_now = $form_state['values']['ab_config_form']['set_now'];
  $vocab_array = $form_state['values']['ab_config_form']['vocab_array'];
  if (!empty($vocab_array)) {
    foreach ($vocab_array as $vid) {
      $tid_array = $form_state['values']['ab_config_form']['vocab_options']['vocab'][$vid]['term_array'];
      foreach ($tid_array as $tid) {
        if (isset($form_state['values']['ab_config_form']['vocab_options']['vocab'][$vid]['terms'])) {
          if ($form_state['values']['ab_config_form']['vocab_options']['vocab'][$vid]['terms'][$tid] == TRUE) {
            $tid_checked[] = $tid;
          }
        }
      }
    }
  }

  $book_array = $form_state['values']['ab_config_form']['book_array'];
  $delta      = (int)$form_state['values']['ab_config_form']['delta'];
  $title      = check_plain($form_state['values']['ab_config_form']['title']);
  $add_mode   = $form_state['values']['ab_config_form']['add_mode'];
  if (!empty($tid_checked)) {
    $tid_string = implode(',', $tid_checked);
  }
  db_delete('adv_book_custom')
  ->condition('delta', $delta)
  ->execute();
  foreach ($book_array as $bid) {
    $bid = (int)$bid;
    $enabled = $form_state['values']['ab_config_form']['book'][$bid]['enabled'];
    $weight_reset = $form_state['values']['ab_config_form']['book'][$bid]['weight_reset'];
    if (empty($weight_reset)) {
      $weight = $form_state['values']['ab_config_form']['book'][$bid]['weight'];
    }
    if (empty($weight)) {
      $weight = 0;
    }

    db_insert('adv_book_custom')
      ->fields(array(
        'delta' => $delta,
        'bid' => $bid,
        'enabled' => $enabled,
        'weight' => $weight,
      ))
      ->execute();
    db_update('adv_book_block')
      ->fields(array('custom' => 1, 'add_mode' => $add_mode, 'title' => $title, 'tids' => $tid_string))
      ->condition('delta', $delta)
      ->execute();
  }
  if ($set_now == TRUE && ($add_mode == 2 || $add_mode == 3 ) && !empty($tid_checked)) {
      foreach ($book_array as $bid) {
        $node_terms = array();
        $result = db_select('taxonomy_index', 'tn')
          ->fields('tn', array('tid'))
          ->condition('nid', $bid)
          ->addTag('node_access')
          ->execute();
        foreach ($result as $term) {
          $node_terms[] = $term->tid;
        }
        if (!empty($node_terms)) {
          if (array_intersect($node_terms, $tid_checked) != NULL && $add_mode == 2) {
            $enabled_set = 1;
          }
          if (array_intersect($node_terms, $tid_checked) == NULL && $add_mode == 2) {
            $enabled_set = 0;
          }
          if (array_intersect($node_terms, $tid_checked) != NULL && $add_mode == 3) {
            $enabled_set = 0;
          }
          if (array_intersect($node_terms, $tid_checked) == NULL && $add_mode == 3) {
            $enabled_set = 1;
          }
          db_update('adv_book_custom')
            ->fields(array('enabled' => $enabled_set))
            ->condition('delta', $delta)
            ->condition('bid', $bid)
            ->execute();
        }
      }

    }
  drupal_set_message(t('Your changes have been saved'), $type = 'status', $repeat = FALSE );
  drupal_goto('admin/config/user-interface/advanced-book-blocks');
}
